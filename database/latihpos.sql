-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 26 Feb 2016 pada 16.21
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `latihpos`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_identitaskel`
--

CREATE TABLE IF NOT EXISTS `t_identitaskel` (
  `No_Rider` varchar(25) NOT NULL,
  `No_Ktp` varchar(25) NOT NULL,
  `Nama` char(20) NOT NULL,
  `Alamat` varchar(30) NOT NULL,
  `No_KK` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_identitaskel`
--

INSERT INTO `t_identitaskel` (`No_Rider`, `No_Ktp`, `Nama`, `Alamat`, `No_KK`) VALUES
('RD 201', '9987889898608', 'Muhammad Supriadi', '', '7781982768'),
('RD 220', '9901872019212', 'Adi Prakoso', 'Jl Sultan Maharani iskandar ', '0928719920');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_rider`
--

CREATE TABLE IF NOT EXISTS `t_rider` (
  `No_Rider` varchar(25) NOT NULL,
  `No_Ktp` varchar(25) NOT NULL,
  `Nama` varchar(25) NOT NULL,
  `TTL` varchar(35) NOT NULL,
  `Jenis_Kelamin` char(10) NOT NULL,
  `Status_Pernikahan` char(20) NOT NULL,
  `Alamat` longtext NOT NULL,
  `No_Handphone` varchar(12) NOT NULL,
  `Jenis_Kendaraan` varchar(20) NOT NULL,
  `Tahun_Kendaraan` varchar(5) NOT NULL,
  `No_Polisi` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_rider`
--

INSERT INTO `t_rider` (`No_Rider`, `No_Ktp`, `Nama`, `TTL`, `Jenis_Kelamin`, `Status_Pernikahan`, `Alamat`, `No_Handphone`, `Jenis_Kendaraan`, `Tahun_Kendaraan`, `No_Polisi`) VALUES
('76287', '87687', 'wahyuanhasuu', 'kswh78', 'awg', 'uygug', 'uguyg', '098098', 'yg', '2019', '8798huy'),
('RD 201', '9910928718829837', 'wahyuan Hafiz', 'Jakarta 16 Juni 1994', 'Laki - Lak', 'Belum', ' Jl Swadaya Raya jakarta Selatan', '2147483647', 'Yamaha Mio', '2013', 'B 9109 VAs');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_user`
--

CREATE TABLE IF NOT EXISTS `t_user` (
  `user_name` varchar(20) NOT NULL,
  `user_pass` varchar(20) NOT NULL,
  `user_tipe` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_user`
--

INSERT INTO `t_user` (`user_name`, `user_pass`, `user_tipe`) VALUES
('hafiz', '1234', 1),
('herry', '12345', 0),
('maho', '12345', 0),
('wahyu', '12345', 0),
('wahyuan', 'wahyuan123', 0);
('agif', '1234', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_identitaskel`
--
ALTER TABLE `t_identitaskel`
 ADD PRIMARY KEY (`No_Rider`);

--
-- Indexes for table `t_rider`
--
ALTER TABLE `t_rider`
 ADD PRIMARY KEY (`No_Rider`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
 ADD PRIMARY KEY (`user_name`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
