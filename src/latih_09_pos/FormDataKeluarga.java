/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package latih_09_pos;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author wahyuan
 */
public class FormDataKeluarga extends javax.swing.JFrame {
    FormIdentitasKeluarga formIdentitasKeluarga;
    String status, id;
    
    /**
     * Creates new form FormJenis
     */
    public FormDataKeluarga() {
        initComponents();
    }
    
    public FormDataKeluarga(FormIdentitasKeluarga formIdentitasKeluarga) {
        initComponents();
        this.formIdentitasKeluarga=formIdentitasKeluarga;
        status = "SIMPAN";
        tombolSimpan.setText("Simpan");
        textNoRider.enable();
        this.setTitle("Tambah Data Identitas Keluarga");
        isiCombo("");
    }
    
    public FormDataKeluarga(FormIdentitasKeluarga formIdentitasKeluarga, String id) {
        initComponents();
        this.formIdentitasKeluarga=formIdentitasKeluarga;
        this.id=id;
        status = "UPDATE";
        tombolSimpan.setText("Update");
        textNoRider.disable();
        this.setTitle("Edit Data Keluarga");
        isiData(id);
    }
    
    public void isiCombo(String idJenis) {
        AksiDB aksidb = new AksiDB();
        String query;
        int i, indek;
        i=0;
        indek=0;
        query ="SELECT * FROM t_identitaskel ORDER BY No_Rider";        
        try{
            Class.forName("com.mysql.jdbc.Driver");
        }catch(Exception ex){
            System.out.println("Driver tidak ditemukan... " + ex);
        }
        try {
            aksidb.dbCon = DriverManager.getConnection(aksidb.dbURL, aksidb.username, aksidb.password);
            aksidb.stmt = aksidb.dbCon.createStatement();
            aksidb.rs = aksidb.stmt.executeQuery(query);
            while(aksidb.rs.next()){
                {
                    if(idJenis.equals(aksidb.rs.getString("No_Rider"))){
                        indek=i;
                    }
                }
                i++;
            }           
        }catch (SQLException ex) {
            Logger.getLogger(FormDataKeluarga.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
        }finally{
            try {
                aksidb.rs.close();
                aksidb.stmt.close();
                aksidb.dbCon.close();
            } catch (SQLException ex) {
                Logger.getLogger(FormDataKeluarga.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void isiData(String cari) {
        AksiDB aksidb = new AksiDB();
        String query ="SELECT * FROM t_identitaskel WHERE No_Rider = '" + cari + "'";
        try{
            Class.forName("com.mysql.jdbc.Driver");
        }catch(Exception ex){
            System.out.println("Driver tidak ditemukan... " + ex);
        }
        try {
            aksidb.dbCon = DriverManager.getConnection(aksidb.dbURL, aksidb.username, aksidb.password);
            aksidb.stmt = aksidb.dbCon.createStatement();
            aksidb.rs = aksidb.stmt.executeQuery(query);
            while(aksidb.rs.next()){
                textNoRider.setText(aksidb.rs.getString("No_Rider"));
                textKode.setText(aksidb.rs.getString("No_Ktp"));
                textNama.setText(aksidb.rs.getString("Nama"));
                isiCombo(aksidb.rs.getString("Alamat"));
                textKK.setText(aksidb.rs.getString("No_KK"));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        } finally{
            try {
                 aksidb.rs.close();
                 aksidb.stmt.close();
                 aksidb.dbCon.close();
             } catch (SQLException ex) {
                 Logger.getLogger(FormDataKeluarga.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
    }
    
    private String getKodeJenis(){
        
        return null;
    }
    
    public void simpanData() {
        AksiDB aksidb = new AksiDB();
        String query ="INSERT INTO t_identitaskel(No_Rider, No_Ktp, Nama, Alamat, No_KK)" 
                + "VALUES('" 
                + textNoRider.getText() + "', '" 
                + textKode.getText() + "', '" 
                + textNama.getText() + "', '" 
                + TextAreaAlamat.getText() + "', "
                + textKK.getText() + " )";
        try{
            Class.forName("com.mysql.jdbc.Driver");
        }catch(Exception ex){
            System.out.println("Driver tidak ditemukan... " + ex);
        }   
        try {
            aksidb.dbCon = DriverManager.getConnection(aksidb.dbURL, aksidb.username, aksidb.password);
            aksidb.stmt = aksidb.dbCon.createStatement();
            aksidb.stmt.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex);
        } finally{
            try {
                 aksidb.stmt.close();
                 aksidb.dbCon.close();
             } catch (SQLException ex) {
                 Logger.getLogger(FormDataKeluarga.class.getName()).log(Level.SEVERE, null, ex);
             }
            dispose();
            formIdentitasKeluarga.tampilData("");
            JOptionPane.showMessageDialog(null,"Data Sudah di-Simpan.", "Konfirmasi",JOptionPane.INFORMATION_MESSAGE );
        }
    }  
    
    public void updateData() {
        AksiDB aksidb = new AksiDB();        
        String query ="UPDATE t_identitaskel SET "
                + "No_Rider = '" + textNoRider.getText()+ "', " 
                + "No_Ktp = '" + textKode.getText() + "', " 
                + "Nama = '" + textNama.getText() + "', " 
                + "Alamat = '" + TextAreaAlamat.getText() + "', "
                + "No_KK = '" + textKK.getText() + "' " 
                
                + " WHERE No_Rider = '" + textNoRider.getText() + "'";
        System.out.println(query);
        try{
            Class.forName("com.mysql.jdbc.Driver");
        }catch(Exception ex){
            System.out.println("Driver tidak ditemukan... " + ex);
        }   
        try {
            aksidb.dbCon = DriverManager.getConnection(aksidb.dbURL, aksidb.username, aksidb.password);
            aksidb.stmt = aksidb.dbCon.createStatement();
            aksidb.stmt.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex);
        } finally{
            try {
                 aksidb.stmt.close();
                 aksidb.dbCon.close();
             } catch (SQLException ex) {
                 Logger.getLogger(FormDataKeluarga.class.getName()).log(Level.SEVERE, null, ex);
             }
            dispose();
            formIdentitasKeluarga.tampilData("");
            JOptionPane.showMessageDialog(null,"Data Sudah di-Update.", "Konfirmasi",JOptionPane.INFORMATION_MESSAGE );
        }
    }  
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        textKode = new javax.swing.JTextField();
        textNama = new javax.swing.JTextField();
        tombolKeluar = new javax.swing.JButton();
        tombolSimpan = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        textKK = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TextAreaAlamat = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        textNoRider = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Input Identitas Keluarga");

        jLabel1.setText("No. KTP :");

        jLabel2.setText("Nama :");

        tombolKeluar.setText("Batal");
        tombolKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolKeluarActionPerformed(evt);
            }
        });

        tombolSimpan.setText("Simpan");
        tombolSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolSimpanActionPerformed(evt);
            }
        });

        jLabel4.setText("Alamat :");

        jLabel5.setText("No. KK :");

        TextAreaAlamat.setColumns(20);
        TextAreaAlamat.setRows(5);
        jScrollPane1.setViewportView(TextAreaAlamat);

        jLabel3.setFont(new java.awt.Font("Stencil Std", 0, 14)); // NOI18N
        jLabel3.setText("IDENTITAS KELUARGA");

        jLabel6.setText("No_Rider :");

        textNoRider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textNoRiderActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(440, 440, 440)
                        .addComponent(tombolSimpan)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tombolKeluar))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(176, 176, 176)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel2)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jScrollPane1)
                                .addComponent(textNama, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel3)
                                .addComponent(textKode, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(textKK, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textNoRider, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel3)
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(textNoRider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(textKode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textNama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textKK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tombolKeluar)
                    .addComponent(tombolSimpan))
                .addGap(32, 32, 32))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tombolSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolSimpanActionPerformed
        if(status.equals("UPDATE")){
            updateData();
        }else{
            simpanData();
        }
    }//GEN-LAST:event_tombolSimpanActionPerformed

    private void tombolKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolKeluarActionPerformed
        dispose();
    }//GEN-LAST:event_tombolKeluarActionPerformed

    private void textNoRiderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textNoRiderActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textNoRiderActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormDataKeluarga.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormDataKeluarga.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormDataKeluarga.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormDataKeluarga.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormDataKeluarga().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea TextAreaAlamat;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField textKK;
    private javax.swing.JTextField textKode;
    private javax.swing.JTextField textNama;
    private javax.swing.JTextField textNoRider;
    private javax.swing.JButton tombolKeluar;
    private javax.swing.JButton tombolSimpan;
    // End of variables declaration//GEN-END:variables
}
