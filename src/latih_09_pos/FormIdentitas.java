/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package latih_09_pos;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author wahyuan
 */
public class FormIdentitas extends javax.swing.JFrame {
    FormIdentitasRider formIdentitasRider;
    String status, id;
    
    /**
     * Creates new form FormIdentitas
     */
    public FormIdentitas() {
        initComponents();
    }
    
    public FormIdentitas(FormIdentitasRider formIdentitasRider) {
        initComponents();
        
        this.formIdentitasRider=formIdentitasRider;
        status = "SIMPAN";
        tombolSimpan.setText("Simpan");
        textNoRider.enable();
        this.setTitle("Tambah Identitas Driver");
        
    }
    
    public FormIdentitas(FormIdentitasRider formIdentitasRider, String id) {
        initComponents();
        this.formIdentitasRider=formIdentitasRider;
        this.id=id;
        status = "UPDATE";
        tombolSimpan.setText("Update");
        textNoRider.disable();
        this.setTitle("Edit Identitas Driver"); 
        isiData(id);
    }
    
    public void isiData(String cari) {
        AksiDB aksidb = new AksiDB();
        String query ="SELECT * FROM t_rider WHERE No_Rider = '" + cari + "'";
        try{
            Class.forName("com.mysql.jdbc.Driver");
        }catch(Exception ex){
            System.out.println("Driver tidak ditemukan... " + ex);
        }
        try {
            aksidb.dbCon = DriverManager.getConnection(aksidb.dbURL, aksidb.username, 
                    aksidb.password);
            aksidb.stmt = aksidb.dbCon.createStatement();
            aksidb.rs = aksidb.stmt.executeQuery(query);
            while(aksidb.rs.next()){
                
                textNoRider.setText(aksidb.rs.getString("No_Rider"));
                textKode.setText(aksidb.rs.getString("No_Ktp"));
                textNama.setText(aksidb.rs.getString("Nama"));
                textTTl.setText(aksidb.rs.getString("TTL"));
                TextKelamin.setText(aksidb.rs.getString("Jenis_Kelamin"));
                TextStatus.setText(aksidb.rs.getString("Status_Pernikahan"));
                TextAreaAlamat.setText(aksidb.rs.getString("Alamat"));
                TextHp.setText(aksidb.rs.getString("No_Handphone"));
                TextKendaraan.setText(aksidb.rs.getString("Jenis_Kendaraan"));
                TextTahun.setText(aksidb.rs.getString("Tahun_Kendaraan"));
                TextNopol.setText(aksidb.rs.getString("No_Polisi"));
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        } finally{
           try {
                 aksidb.rs.close();
                 aksidb.stmt.close();
                 aksidb.dbCon.close();
             } catch (SQLException ex) {
                 Logger.getLogger(FormIdentitas.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
    }
    
    public void simpanData() {
        AksiDB aksidb = new AksiDB();
        String query ="INSERT INTO t_rider(No_Rider, No_Ktp, Nama, TTL, Jenis_Kelamin, Status_Pernikahan, Alamat, No_Handphone, Jenis_Kendaraan, Tahun_Kendaraan, No_Polisi) " 
                + "VALUES('" 
                + textNoRider.getText() + "', '" 
                + textKode.getText() + "', '" 
                + textNama.getText() + "', '" 
                + textTTl.getText() + "', '"
                + TextKelamin.getText() + "', '" 
                + TextStatus.getText() + "', '" 
                + TextAreaAlamat.getText() + "', '" 
                + TextHp.getText() + "', '" 
                + TextKendaraan.getText() + "', '" 
                + TextTahun.getText() + "', '" 
                + TextNopol.getText() + "')";
        System.out.println(query);
        try{
            Class.forName("com.mysql.jdbc.Driver");
        }catch(Exception ex){
            System.out.println("Driver tidak ditemukan... " + ex);
        }   
        try {
            aksidb.dbCon = DriverManager.getConnection(aksidb.dbURL, aksidb.username, 
                    aksidb.password);
            aksidb.stmt = aksidb.dbCon.createStatement();
            aksidb.stmt.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex);
        } finally{
            try {
                 aksidb.stmt.close();
                 aksidb.dbCon.close();
             } catch (SQLException ex) {
                 Logger.getLogger(FormIdentitas.class.getName()).log(Level.SEVERE, null, ex);
             }
            dispose();
            formIdentitasRider.tampilData("");
            JOptionPane.showMessageDialog(null,"Data Sudah di-Simpan.", 
                    "Konfirmasi",JOptionPane.INFORMATION_MESSAGE );
        }
    }  
    
    public void updateData() {
        AksiDB aksidb = new AksiDB();
        String query ="UPDATE t_rider SET "
                + "No_Ktp = '" + textKode.getText() + "', " 
                + "Nama = '" + textNama.getText() + "', " 
                + "TTL = '" + textTTl.getText() + "', "  
                + "Jenis_Kelamin = '" + TextKelamin.getText() + "', " 
                + "Status_Pernikahan = '" + TextStatus.getText() + "', " 
                + "Alamat = '" + TextAreaAlamat.getText() + "', " 
                + "No_Handphone = '" + TextHp.getText() + "', " 
                + "Jenis_Kendaraan = '" + TextKendaraan.getText() + "', " 
                + "Tahun_Kendaraan = '" + TextTahun.getText() + "', " 
                + "No_Polisi = '" + TextNopol.getText() + "'" 
                + " WHERE No_Rider = '" + textNoRider.getText() + "'";
        System.out.println(query);
        try{
            Class.forName("com.mysql.jdbc.Driver");
        }catch(Exception ex){
            System.out.println("Driver tidak ditemukan... " + ex);
        }   
        try {
            aksidb.dbCon = DriverManager.getConnection(aksidb.dbURL, aksidb.username, 
                    aksidb.password);
            aksidb.stmt = aksidb.dbCon.createStatement();
            aksidb.stmt.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex);
        } finally{
            try {
                 aksidb.stmt.close();
                 aksidb.dbCon.close();
             } catch (SQLException ex) {
                 Logger.getLogger(FormIdentitas.class.getName()).log(Level.SEVERE, null, ex);
             }
            dispose();
            formIdentitasRider.tampilData("");
            JOptionPane.showMessageDialog(null,"Data Sudah di-Update.", 
                    "Konfirmasi",JOptionPane.INFORMATION_MESSAGE );
        }
    }  
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        textKode = new javax.swing.JTextField();
        textNama = new javax.swing.JTextField();
        textTTl = new javax.swing.JTextField();
        tombolKeluar = new javax.swing.JButton();
        tombolSimpan = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        TextKelamin = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        TextStatus = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TextAreaAlamat = new javax.swing.JTextArea();
        jLabel7 = new javax.swing.JLabel();
        TextHp = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        TextKendaraan = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        TextTahun = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        TextNopol = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        textNoRider = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Input Data Jenis");

        jLabel1.setText("No. KTP");

        jLabel2.setText("Nama Lengkap");

        jLabel3.setText("TTL");

        tombolKeluar.setText("Batal");
        tombolKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolKeluarActionPerformed(evt);
            }
        });

        tombolSimpan.setText("Simpan");
        tombolSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolSimpanActionPerformed(evt);
            }
        });

        jLabel4.setText("Jenis Kelamin");

        TextKelamin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TextKelaminActionPerformed(evt);
            }
        });

        jLabel5.setText("Status Pernikahan");

        jLabel6.setText("Alamat");

        TextAreaAlamat.setColumns(20);
        TextAreaAlamat.setRows(5);
        jScrollPane1.setViewportView(TextAreaAlamat);

        jLabel7.setText("No HandPhone");

        jLabel8.setText("Jenis Kendaraan");

        jLabel9.setText("Tahun Kendaraan");

        TextTahun.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TextTahunActionPerformed(evt);
            }
        });

        jLabel10.setText("No. Pol");

        jLabel11.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        jLabel11.setText("IDENTITAS RIDER");

        jLabel12.setText("No. Rider ");

        textNoRider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textNoRiderActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(tombolSimpan)
                .addGap(54, 54, 54)
                .addComponent(tombolKeluar)
                .addGap(121, 121, 121))
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel9)
                            .addComponent(jLabel8)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(TextTahun, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(TextNopol, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(TextKendaraan, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(TextHp, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textNama, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textTTl, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(TextStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(TextKelamin, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(textNoRider)
                                    .addComponent(textKode, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel11))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(146, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(textNoRider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(textKode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textNama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textTTl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(TextKelamin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(TextStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(0, 49, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(TextHp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(TextKendaraan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(TextTahun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(TextNopol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tombolSimpan)
                    .addComponent(tombolKeluar))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tombolSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolSimpanActionPerformed
 if(status.equals("UPDATE")){
            updateData();
        }else{
            simpanData();
        }
    }//GEN-LAST:event_tombolSimpanActionPerformed

    private void tombolKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolKeluarActionPerformed
        dispose();
    }
         public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormIdentitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormIdentitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormIdentitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormIdentitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormIdentitas().setVisible(true);
            }
        });
    

    }//GEN-LAST:event_tombolKeluarActionPerformed

    private void TextKelaminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TextKelaminActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TextKelaminActionPerformed

    private void TextTahunActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TextTahunActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TextTahunActionPerformed

    private void textNoRiderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textNoRiderActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textNoRiderActionPerformed

    /**
     * @param args the command line arguments
     */
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea TextAreaAlamat;
    private javax.swing.JTextField TextHp;
    private javax.swing.JTextField TextKelamin;
    private javax.swing.JTextField TextKendaraan;
    private javax.swing.JTextField TextNopol;
    private javax.swing.JTextField TextStatus;
    private javax.swing.JTextField TextTahun;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField textKode;
    private javax.swing.JTextField textNama;
    private javax.swing.JTextField textNoRider;
    private javax.swing.JTextField textTTl;
    private javax.swing.JButton tombolKeluar;
    private javax.swing.JButton tombolSimpan;
    // End of variables declaration//GEN-END:variables
}
